# Chinchilla

## About

Chinchilla is a web-based c runtime and development environment aimed at teaching programming to youth

## History

* 2024:
    - Continued development
    - Ported to Vite
    - License changed to the GPL
* 2020: 
    - Began development as student project
    - Project dormant

## Authors

- Jeremy McCubbin
- Marcus Minhorst
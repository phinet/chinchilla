const handler = {};

handler["variable declaration"] = var_decl(data);
handler["for loop"] = for_loop(data);

handler[data.keyword](data);
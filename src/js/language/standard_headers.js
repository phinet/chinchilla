import * as stdio from "./headers/stdio";
import * as stdlib from "./headers/stdlib";

export const init = () =>
{
    stdio.init();
    stdlib.init();
};

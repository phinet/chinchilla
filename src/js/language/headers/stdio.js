import { blocks } from "../../ui/editor/elements";
import * as Literals from "../../runtime/literals";
import * as runtime from "../../runtime/runtime";
import create_function from "../../ui/editor/create_function";
import { io } from "./../../ui/console/io";

export const init = () =>
{
    const create = (x) =>
    {
        if (x.varargs) x.args.varargs = true;

        create_function(x.name, x.type, x.args, x.handler, "Standard I/O", "standard_library");
    };

    const puts = (callback, str) =>
    {
        io.printtoConsole(str + "\n");

        callback(1);
    };

    const gets = (callback, address) =>
    {
        io.readFromConsole((str) =>
        {
            runtime.access_variable(address).value = str;

            callback(1);
        });
    };

    const getchar = (callback) =>
    {
        console.log("getchar");

        io.readFromConsole((str) =>
        {
            callback(str[0]);
        });
    };

    const printf = (callback, format, ...args) =>
    {
        let split = format.split("%%");

        const regex = /%[cdieEfgGosuxXpn]/;

        let replaced = true;

        for (let i = 0; i < args.length; i++)
        {
            replaced = false;

            for (let j = 0; j < split.length; j++)
            {
                if (regex.test(split[j]))
                {
                    replaced = true;
                    split[j] = split[j].replace(regex, args[i]);

                    break;
                }
            }

            if (!replaced) throw "Invalid format string";
        }

        const joined = split.join("%");
        const unescaped = Literals.string(joined);

        io.printtoConsole(unescaped);

        callback(unescaped.length);
    };

    const definitions = [{
        name: "puts",
        type: "int",
        args: [ { name: "str", type: "char *" } ],
        varargs: false,
        handler: puts
    }, {
        name: "gets",
        type: "int",
        args: [ { name: "str", type: "char *" } ],
        varargs: false,
        handler: gets
    }, {
        name: "getchar",
        type: "int",
        args: [ ],
        varargs: false,
        handler: getchar
    }, {
        name: "printf",
        type: "int",
        args: [ { name: "format", type: "char *" } ],
        varargs: true,
        handler: printf
    }];

    definitions.forEach(create);
};

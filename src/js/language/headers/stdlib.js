import * as runtime from "../../runtime/runtime";
import create_function from "../../ui/editor/create_function";

export const init = () =>
{
    const create = (x) =>
    {
        if (x.varargs) x.args.varargs = true;

        create_function(x.name, x.type, x.args, x.handler, "Standard Library", "standard_library");
    };

    const atoi = (callback, buffer) =>
    {
        callback(parseInt(buffer));
    };

    const malloc = (callback, size) =>
    {
        callback(runtime.memory_alloc(size));
    };

    const free = (callback, address) =>
    {
        // lmao i love memory leaks
        // runtime.memory_free(address);

        callback();
    };

    const definitions = [{
        name: "atoi",
        type: "int",
        args: [ { name: "buffer", type: "char *" } ],
        varargs: false,
        handler: atoi
    }, {
        name: "malloc",
        type: "void *",
        args: [ { name: "size", type: "size_t" } ],
        varargs: false,
        handler: malloc
    }, {
        name: "free",
        type: "void",
        args: [ { name: "address", type: "size_t" } ],
        varargs: false,
        handler: free
    }];

    definitions.forEach(create);
};

import * as standard_headers from "./standard_headers";
import * as create_function from "./../ui/editor/create_function";

export const init = () =>
{
    standard_headers.init();
    create_function.init();
};

/**
 * Cookie handling code
 *
 * To set cookie:
 * set(key, value, [max_age, [path]])
 *
 * To get cookie:
 * get(key)
 *
 * To clear cookie:
 * clear(key, [path])
 */

const parse_path = (path) =>
{
    if (typeof path !== "string") return ";path=/";

    return ";path=" + path;
};

const parse_age = (age) =>
{
    if (typeof age !== "number") return "";

    if (age < 0) return ";Expires=Thu, 01 Jan 1970 00:00:01 GMT";

    return ";max-age=" + Math.floor(age);
};

const create = (key, value, max_age, path) => key + "=" + value + parse_age(max_age) + parse_path(path);

export const set = (key, value, max_age, path) => document.cookie = create(key, value, max_age, path);

export const clear = (key, path) => set(key, "", 0, path);

export const get = (key) =>
{
    const prefix = key + "=";
    const cookies = document.cookie.split(";");

    for (let cookie of cookies)
    {
        cookie = cookie.trim();

        if (cookie.startsWith(prefix))
        {
            return cookie.substring(prefix.length, cookie.length);
        }
    }

    return null;
};

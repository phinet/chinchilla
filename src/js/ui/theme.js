export const theme = {
    set: (key, value) =>
    {
        if (typeof key === "string")
        {
            if (value === null || value === undefined)
            {
                delete theme.data[key];

                document.documentElement.style.setProperty("--" + key, "");
            }

            theme.data[key] = value;

            document.documentElement.style.setProperty("--" + key, "" + value);
        }
        else
        {
            for (const k in key)
            {
                theme.set(k, key[k]);
            }
        }
    },

    get: (key) =>
    {
        return theme.data[key];
    },

    data: {}
};

import { create_element } from "../../util/create_element";
import { ui } from "../create_gui";

export const io = {};

export const start = () =>
{
//Variables

    var inputBuffer = [];//buffer

    const editable = "removeable";
    const noneditable = "nonremovable";

    //Enable consol window events
    ui.console.setAttribute("tabindex", 0);

    // Method exposure
    //printtoConsole displays string provided as uneditable text
    io.printtoConsole = (text) =>
    {
        outputHandeler(text, "program");
    };

    let awaiting_callback;

    //Method to read buffer
    //First argument is a callback that recieves one argument
    io.readFromConsole = (callback) =>
    {
        if (inputBuffer.length > 0)
        {
            callback(inputBuffer.shift());
        }
        else
        {
            awaiting_callback = callback;
        }
    };

    //removes all child elements of the body div in console
    io.clearConsole = clearConsole;

    //Sets buffers to [] to clear
    io.clearBuffer = clearBuffer;

    const header = create_element({
        tag: "h2",
        content: "Console Data:"
    });

    const body = create_element({
        tag: "div"
    });

    //Window Keypress Listener
    ui.console.addEventListener("keydown", function(e)
    {
        e.preventDefault();
        console.log("KeyPressed:" + e.key);
        outputHandeler(e.key, "keyboard");

        return false;
    });

    //Append HTML Elements
    ui.console.appendChild(header);
    ui.console.appendChild(body);

    //test program
    //TestProgramOutput();

    function TestProgramOutput()
    {
        let string = " a b c d e f g h i j k l m n o p q r s t u v w x y z ,./?><1234567890-=`~!@#$%^&*()_+";

        outputHandeler(string, "program");

        string = "Special Characters\n \a \e\ \b  \t  \v";

        outputHandeler(string, "program");
    }


    function outputHandeler(string, source)
    {
        let control = null;

        if (source === "keyboard")
        {
            if (string.length>1)
            {
                control = true;
            }

            //Switch Statement to handel control strings from Keyboard
            switch (string)
            {
                case "Enter":
                    pushtoBuffer();
                    displayCharacter(string, control, source);

                    //
                    break;
                case "Shift":
                    break;
                case "Backspace":
                    removelastCharacter();

                    break;
                case "Delete":
                    break;
                case "ArrowLeft":
                case "ArrowRight":
                case "ArrowDown":
                case "ArrowUp":
                //Do nothing for Arrow Keys;
                    break;
                case "Shift":
                    break;
                default:
                    displayCharacter(string, control, source);
            }
        }//end if keyed input
        else if (source === "program")
        { //idea
            for (const ch of string)
            {
                switch (ch)
                {
                /*
                case '\\a':
                    //Alert
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\b':
                    //Backspace
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\\e':
                    //Escape Character
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\f':
                    //Formfeed Pagebreak
                    console.log("Escape:" + ch);
                    control=true;
                break;
                */
                    case "\n":
                    //New line
                        console.log("Escape:" + ch);
                        control=true;

                        break;

                        /*
                case '\r':
                    //Carrage Return
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\t':
                    //Tab
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\v':
                    //Vertical Tab
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\\':
                    //Backslash
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\'':
                    //Apostrophe
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\"':
                    //Double Quote
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\?':
                    //Questionmark
                    console.log("Escape:" + ch);
                    control=true;
                break;
                */
                        /*
                case '\nnn':
                    //byte from octal
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\xhh':
                    //byte from hex
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\uhhhh':
                    //Hex
                    console.log("Escape:" + ch);
                    control=true;
                break;
                case '\uhhhhhhhh':
                    //Hex
                    console.log("Escape:" + ch);
                    control=true;
                break;
                */
                    default:
                        control = false;
                }//end switch

                displayCharacter(ch, control, source);
            } //end for
        }
    }

    //removes all child elements of the body div in console
    function clearConsole ()
    {
        while (body.lastChild)
        {
            body.removeChild(body.lastChild);
        }
    }

    //Sets buffers to [] to clear
    function clearBuffer()
    {
        inputBuffer =[];
    }

    function removelastCharacter ()
    {
        if (body.lastChild && body.lastChild.className === editable)
        {
            let temp = body.lastChild;

            body.removeChild(body.lastChild);

            return temp;
        }
        else
        {
            return false;
        }
    }

    function pushtoBuffer()
    {
    //each unprotected value
        let temp;
        let string ="";

        while (temp=removelastCharacter())
        {
            string=temp.textContent.concat(string);
        }

        //Reprint Protected
        displayCharacter(string, null, "Buffer");


        inputBuffer.push(string);

        if (awaiting_callback)
        {
            awaiting_callback(inputBuffer.shift());
            awaiting_callback = undefined;
        }

        console.log(inputBuffer);

    //push to buffer
    }

    function displayCharacter (string, control, source)
    {
    //barely-functional special Character detector
        var outputText;

        let editClass;

        if (source === "keyboard")
        {
            editClass=editable;
        }
        else
        {
            editClass=noneditable;
        }

        if (control)
        {
            switch (string)
            {
                case "Enter":
                case "\n":
                    outputText = create_element({
                        tag: "span br",
                        class: noneditable //cannot remove once on new line
                    });

                    break;
                default:
                    return;
            }
        }
        else
        {
            outputText = create_element({
                tag: "span",
                content: string,
                class: editClass
            });
        }

        let temp = (ui.console.scrollHeight - ui.console.scrollTop) - ui.console.clientHeight;


        body.appendChild(outputText);

        const rem = parseFloat(getComputedStyle(document.documentElement).fontSize);

        if ( temp < rem+1 )
        {
            ui.console.scrollTop =ui.console.scrollHeight;
        }
    }
};

import { theme } from "./../ui/theme";
import * as Cookie from "./../util/cookies";
import { create_element } from "../util/create_element";

// create all ui components
const create_elements = () =>
{
    // START EDITOR

    ui.code = create_element({ tag: "div", id:    "code", content: "code" });
    ui.pallete = create_element({ tag: "div", id: "pallete", content: { tag: "h1", content: "Pallete" } });
    ui.editor = create_element({ tag: "div", id:  "editor" });

    ui.pallete_container = create_element({ tag: "div", id: "pallete-container", class: "resize-ew", content: ui.pallete });

    ui.edit_pane = [ ui.code, ui.pallete_container, ui.editor ];

    ui.edit_pane = create_element({ tag: "div", id: "edit-pane", class: "resize-ew", content: ui.edit_pane });

    // END EDITOR

    // START INFO

    ui.error = create_element({ tag: "div", id: "runtime-error" });
    ui.memory = create_element({ tag: "div", id: "runtime-memory" });

    ui.docs = create_element({ tag: "div", id:    "docs", content:    "docs" });
    ui.info = create_element({ tag: "div", id:    "info", content: [ { tag: "h1", content: "info" }, ui.error, ui.memory ] });
    ui.console = create_element({ tag: "div", id: "console" });

    ui.console_container = create_element({ tag: "div", id: "console-container", class: "resize-ns", content: ui.console });

    ui.info_pane = [ ui.docs, ui.console_container, ui.info ];

    ui.info_pane = create_element({ tag: "div", id: "info-pane", content: ui.info_pane });

    // END INFO

    ui.toolbar = [
        "toolbar ",
        {tag: "a#codeview?href=#", content: "code", events: { click: () => ui.edit_pane.classList.add("code-view") }},
        " ",
        {tag: "a?href=#", content: "editor", events: { click: () => ui.edit_pane.classList.remove("code-view") }},
        " ",
        {tag: "a?href=#", content: "docs", events: { click: () => ui.info_pane.classList.add("docs-view") }},
        " ",
        {tag: "a?href=#", content: "console", events: { click: () => ui.info_pane.classList.remove("docs-view") }}
    ];

    ui.tabs = [ "tabs" ];
    ui.content = [ ui.edit_pane, ui.info_pane ];
    ui.footer = [ "footer" ];

    console.log(ui.toolbar);

    ui.toolbar = create_element({ tag: "header nav", id: "toolbar", content: ui.toolbar });
    ui.tabs = create_element({ tag: "nav", id:    "tabs", content: ui.tabs });
    ui.content = create_element({ tag: "main", id: "content", content: ui.content });
    ui.footer = create_element({ tag: "footer nav", id:  "footer", content: ui.footer });
};

const register_resize_events = () =>
{
    // register events that handle resizing of panes

    let drag_element, drag_size, drag_start, drag_event;

    // saved or default sizes
    let pane_sizes = JSON.parse(Cookie.get("pane_sizes")) || {
        "edit-pane": (window.innerWidth * 3 / 4) + "px",
        "pallete-container": (window.innerWidth / 4) + "px",
        "console-container": (window.innerHeight / 3) + "px"
    };

    const set_size = (size) =>
    {
        if (drag_element.id)
        {
            pane_sizes[drag_element.id] = size;

            Cookie.set("pane_sizes", JSON.stringify(pane_sizes), 3e7);
        }
    };

    const resize_ew = {
        type: "width",
        position: "clientX",
        size: "offsetWidth",
        handler: (e) =>
        {
            const size = drag_size + e.clientX - drag_start + "px";

            drag_element.style.width = size;

            set_size(size);
        }
    };

    const resize_ns = {
        type: "height",
        position: "clientY",
        size: "offsetHeight",
        handler: (e) =>
        {
            const size = drag_size + e.clientY - drag_start + "px";

            drag_element.style.height = size;

            set_size(size);
        }
    };

    const handle_drag_event = (e) =>
    {
        if (drag_event) drag_event(e);
    };

    const register_resize_event = (e, type) =>
    {
        if (e.target !== e.currentTarget) return false;

        if (e.button === 0)
        {
            drag_element = e.currentTarget;
            drag_start = e[type.position];
            drag_size = drag_element[type.size];
            drag_event = type.handler;

            return true;
        }
    };


    const cancel_drag_event = () => drag_event = false;

    const prepare_element = (element, type) =>
    {
        if (element.id && pane_sizes[element.id])
        {
            element.style[type.type] = pane_sizes[element.id];
        }

        element.addEventListener("mousedown", (e) => register_resize_event(e, type));
    };

    window.addEventListener("mousemove", handle_drag_event);
    window.addEventListener("mouseup", cancel_drag_event);

    prepare_element(ui.edit_pane, resize_ew);
    prepare_element(ui.pallete_container, resize_ew);
    prepare_element(ui.console_container, resize_ns);
};

export const ui = {};

export const build = () =>
{
    document.title = "Chinchilla";

    theme.set({
        header_colour:      theme.get("theme_a"),
        background_colour:  theme.get("theme_a_lightest"),
        text_colour:        theme.get("theme_a_darkest"),

        foreground_colour:  theme.get("theme_lightest"),
        header_text_colour: theme.get("theme_lightest"),
    });

    create_elements();
    register_resize_events();

    document.body.appendChild(ui.toolbar);
    document.body.appendChild(ui.tabs);
    document.body.appendChild(ui.content);
    document.body.appendChild(ui.footer);
};

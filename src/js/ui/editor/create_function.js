import { blocks } from "./elements";
import { ui } from "../create_gui";
import { create_element } from "../../util/create_element";
import { io } from "./../console/io";
import * as runtime from "../../runtime/runtime";
import * as symbols from "../../symbols";
import state from "../../state";

const create_function = (name, type, args, native, supertype, colors) =>
{
    const def = {
        name: name,
        userdefined: !native,
        builder: blocks.create_block_element,
        colours: colors || "function",
        supertype: supertype || "Functions",
        top: [ type, " ", name, " ("],
        bottom: [ "}" ],
        disable_bottom: true,
        disable_top: true
    };

    const call = {
        name: name,
        builder: blocks.create_inline_element,
        colours: colors || "function",
        supertype: supertype || "Functions",
        top: [ name, "("]
    };

    if (args.length > 0)
    {
        for (const arg of args)
        {
            def.top.push(arg.type, " ", arg.name, ", ");
            call.top.push(arg, ",");
        }
    }

    if (args.varargs)
    {
        def.top.push("...");
        call.top.push("...");
        call.varargs = true;
    }
    else if (args.length > 0)
    {
        def.top.pop();
        call.top.pop();
    }

    def.top.push(") {");
    call.top.push(")");

    if (symbols.functions[name])
    {
        const parent = symbols.functions[name].def.parentElement;

        parent.removeChild(symbols.functions[name].def);
        parent.removeChild(symbols.functions[name].call);
    }

    if (native)
    {
        symbols.native[name] = {
            name: name,
            type: type,
            args: args,
            def: native,
            call: blocks.create_pallete_block(call)
        };
    }
    else
    {
        symbols.functions[name] = {
            name: name,
            type: type,
            args: args,
            def: blocks.create_pallete_block(def),
            call: blocks.create_pallete_block(call)
        };
    }
};

const popup = () =>
{
    const container = create_element({
        tag: "div",
        class: "modal_container"
    });

    const remove = () =>
    {
        document.body.removeChild(container);
    };

    container.onclick = remove;

    const content = create_element({
        tag: "div",
        class: "modal_content",
        onclick: (e) => e.stopPropagation(),
        content: [
            {
                tag: "div",
                class: "create_function_button float_right",
                content: "Close",
                onclick: remove
            },
            {
                tag: "h1",
                content: "Create Function"
            },
        ]
    });

    const enter_details = (name, type, args, varargs) =>
    {
        name = name || "";
        type = type || "void";
        args = args || [];
        varargs = varargs || false;

        const name_field = create_element({ tag: "input", type: "text", value: name });
        const type_field = create_element({ tag: "input", type: "text", value: type });
        const arg_list = create_element({ tag: "div" });
        const varg_box = create_element({ tag: "input.vararg", type: "checkbox", checked: varargs });

        const enter = () =>
        {
            const name = name_field.value.trim();
            const type = type_field.value.trim();

            if (!name || !type) return;

            const args = [];

            const arg_names = arg_list.getElementsByClassName("name");
            const arg_types = arg_list.getElementsByClassName("type");

            for (let i = 0; i < arg_names.length; i++)
            {
                const arg = {
                    name: arg_names[i].value.trim(),
                    type: arg_types[i].value.trim()
                };

                if (!arg.name || !arg.type) return;

                args.push(arg);
            }

            args.varargs = varg_box.checked;

            create_function(name, type, args);

            remove();
        };

        const add_arg = (name, type) =>
        {
            name = name || "";
            type = type || "";

            const count = arg_list.children.length + 1;

            arg_list.appendChild(create_element({
                tag: "div.padded",
                content: [
                    { tag: "label", content: "Argument #" + count + " name: " },
                    { tag: "input.name", type: "text", value: name },
                    { tag: "label", content: " type: " },
                    { tag: "input.type", type: "text", value: type }
                ]
            }));
        };

        const remove_arg = () =>
        {
            if (arg_list.lastChild)
            {
                arg_list.removeChild(arg_list.lastChild);
            }
        };

        for (const arg of args)
        {
            add_arg(arg.name, arg.type);
        }

        content.append(create_element({
            tag: "div",
            content: [
                { tag: "div.padded", content: "Signature:" },
                { tag: "div.indented",
                    content: [
                        { tag: "div.padded",
                            content: [
                                { tag: "label", content: "Function name: " }, name_field
                            ]},
                        { tag: "div.padded",
                            content: [
                                { tag: "label", content: "Return type: " }, type_field
                            ]}
                    ]},
                { tag: "div.padded", content: "Arguments:" },
                { tag: "div.indented",
                    content: [
                        { tag: "div.padded",
                            content: [
                                { tag: "label", content: "Variable number of args? " }, varg_box
                            ]},
                        arg_list,
                        { tag: "div.padded",
                            content: [
                                { tag: "input", type: "button", value: "Add argument", onclick: () => void add_arg() },
                                { tag: "input", type: "button", value: "Remove argument", onclick: () => void remove_arg() }
                            ]}
                    ]},
                { tag: "div", class: "create_function_button", content: "Create", onclick: enter },
                { tag: "div", class: "create_function_button", content: "Cancel", onclick: remove }
            ]
        }));
    };

    const enter_name = (name) =>
    {
        const field = create_element({ tag: "input", type: "text" });

        const enter = (e) =>
        {
            if (e.key && e.key !== "Enter") return;

            const name = field.value.trim();

            if (name.length === 0) return;

            content.removeChild(content.lastChild);

            enter_details(name);
        };

        field.onkeypress = enter;

        content.append(create_element({
            tag: "div",
            content: [
                { tag: "label", content: "Function name: " },
                field,
                { tag: "input", type: "button", value: "Next", onclick: enter }
            ]
        }));
    };

    if (!symbols.functions.main)
    {
        content.appendChild(create_element({
            tag: "div",
            class: "center_content",
            content: [
                {
                    tag: "div",
                    class: "create_function_button",
                    content: "main",
                    onclick: () =>
                    {
                        content.removeChild(content.lastChild);
                        enter_details("main", "int", [{name: "argc", type: "int"}, {name: "argv", type: "char **"}]);
                    }
                },
                {
                    tag: "div",
                    class: "create_function_button",
                    content: "simple main",
                    onclick: () =>
                    {
                        content.removeChild(content.lastChild);
                        enter_details("main");
                    }
                },
                {
                    tag: "div",
                    class: "create_function_button",
                    content: "custom",
                    onclick: () =>
                    {
                        content.removeChild(content.lastChild);
                        enter_name();
                    }
                }
            ]
        }));
    }
    else
    {
        enter_name();
    }

    container.appendChild(content);

    document.body.appendChild(container);
};

const start = (speed) =>
{
    console.log(speed);

    if (!speed || speed < 0) speed = 0;

    console.log(speed);

    runtime.setDelay(speed);

    const error = (m) => void alert(m || "Invalid main function");

    io.clearConsole();

    if (!symbols.functions.main)
    {
        return void error("No main function declared");
    }
    else if (!symbols.functions.main.definition)
    {
        return void error("No main function defined");
    }

    const main = symbols.functions.main;

    if (main.args.varargs || !(main.type === "void" || main.type === "int")) return void error();

    switch (symbols.functions.main.args.length)
    {
        case 2:
            if (main.args[0].type !== "int" || main.args[1].type !== "char **") return void error();
        case 0:
            document.body.classList.add("running");
            state.running = true;

            runtime.run();

            return;
    }

    error();
};

const slow = () =>
{
    console.log("slow");
    start(250);
};

const fast = () =>
{
    console.log("fast");
    start(0);
};

const pause = () =>
{
    document.body.classList.add("paused");
    console.log("pause");
};

const resume = () =>
{
    document.body.classList.remove("paused");
    console.log("resume");
};

const stop = () =>
{
    document.body.classList.remove("running");
    document.body.classList.remove("paused");
    console.log("stop");
    io.clearBuffer();
    state.running = false;
};

const go = (func) =>
{
    requestAnimationFrame(func);

    return false;
};

export const init = () =>
{
    const insertAfter = (p, s, e) =>
    {
        if (s.nextSibling)
        {
            p.insertBefore(e, s.nextSibling);
        }
        else
        {
            p.appendChild(e);
        }
    };

    const create_button = create_element({
        tag: "div",
        class: "create_function_button",
        content: "Create Function",
        onclick: popup
    });

    insertAfter(ui.pallete, ui.pallete.firstChild, create_button);

    const slow_button = create_element({ tag: "a.start", href: "#", content: "Slow", onclick: () => go(slow) });
    const start_button = create_element({ tag: "a.start", href: "#", content: "Start", onclick: () => go(fast) });
    const pause_button = create_element({ tag: "a.pause", href: "#", content: "Pause", onclick: () => go(pause) });
    const resume_button = create_element({ tag: "a.resume", href: "#", content: "Resume", onclick: () => go(resume) });
    const stop_button = create_element({ tag: "a.stop", href: "#", content: "Stop", onclick: () => go(stop) });

    ui.toolbar.inner.append(create_element({
        tag: "div",
        class: "float_right",
        content: [ slow_button, start_button, pause_button, resume_button, stop_button ]
    }));
};

export default create_function;

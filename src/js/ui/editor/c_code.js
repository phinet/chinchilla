import { create_element } from "../../util/create_element";
import { ui } from "../create_gui";
import "../../util/string_format";
import { root } from "./elements";
import * as runtime from "../../runtime/runtime";
import * as symbols from "../../symbols";

export const start = () =>
{
    console.log("text output generator");

    const codeview_button = document.getElementById("codeview");
    const codeview = ui.code;

    const generate_code = (block) =>
    {
        if (block === undefined) return "";
        if (typeof block === "string") return block;

        block = block.data;

        const fields = runtime.get_fields(block).map((x) => generate_code(x));

        const expressions = {
            "plus":      "( {0} + {1} )",
            "minus":     "( {0} - {1} )",
            "times":     "( {0} * {1} )",
            "divide":    "( {0} / {1} )",
            "remainder": "( {0} % {1} )",

            "variable access": "( {0} )",

            "pre increment":  "( ++{0} )",
            "pre decrement":  "( --{0} )",
            "post increment": "( {0}++ )",
            "post decrement": "( {0}-- )",

            "equivalent":                 "( {0} == {1} )",
            "not equivalent":             "( {0} != {1} )",
            "less than or equivalent":    "( {0} <= {1} )",
            "greater than or equivalent": "( {0} >= {1} )",
            "less than":                  "( {0} < {1} )",
            "greater than":               "( {0} > {1} )",

            "variable assignment": "( {0} = {1} )",
            "plus assign":         "( {0} += {1} )",
            "minus assign":        "( {0} -= {1} )",
            "times assign":        "( {0} *= {1} )",
            "divide assign":       "( {0} /= {1} )",
            "remainder assign":    "( {0} %= {1} )",

            "address of":  "( &{0} )",
            "dereference": "( *{0} )",

            "return": "return {0}",

            "variable declaration and assignment": "{0} {1} = {2}",

            "variable declaration": "{0} {1}"

        };

        const blocks = {
            "if":      [ "if ({0}) {", "}" ],
            "else if": [ "else if ({0}) {", "}" ],
            "else":    [ "else {", "}" ],

            "for loop":      [ "for ({0}; {1}; {2}) {", "}" ],
            "while loop":    [ "while ({0}) {", "}" ],
            "do while loop": [ "do {", "} while ({0})" ],
        };

        if (expressions[block.typestring])
        {
            return expressions[block.typestring].format(...fields);
        }

        if (blocks[block.typestring])
        {
            let top = blocks[block.typestring][0];
            let bottom = blocks[block.typestring][1];

            let body = generate_lines(block.body.child, true);

            top = top.format(...fields) + "\n";
            bottom = bottom.format(...fields);

            return top + body + bottom;
        }

        if (block.name)
        {
            return block.name + "(" + fields.join(", ") + ")";
        }

        // switch (block.typestring) {
        //     default: {
        //         if (block.typestring.name) {

        //         }
        //     } break;
        // }

        return "block";
    };

    const generate_lines = (line, indent) =>
    {
        let out = "";

        indent = (indent)? "    ": "";

        if (!line) return indent + "\n";

        do
        {
            out += generate_code(line) + ";\n";
        } while (line = line.child);

        out = out.replace(/};/g, "}");
        out = out.replace(/ +;/g, ";");
        out = out.replace(/; +\)/g, ";)");
        out = out.replace(/}\nelse/g, "} else");
        out = out.replace(/\n/g, "\n" + indent);

        return indent + out.trim() + "\n";
    };

    const start_generating_code = () =>
    {
        let code = "#include <stdio.h>\n#include <stdlib.h>\n\n";

        let prototypes = "";
        let bare_statements = "";
        let functions = "";

        root.forEach((i) =>
        {
            if (i.name && symbols.functions[i.name].definition === i)
            {
                console.log(i.typestring);

                const top = i.typestring.top.join("");
                const bottom = i.typestring.bottom.join("");

                prototypes += top.substring(0, top.length - 2) + ";\n";

                functions += top + "\n";

                functions += generate_lines(i.body.child, true);

                functions += bottom + "\n\n";
            }
            else
            {
                bare_statements += generate_lines(i) + "\n";
            }
        });

        code += prototypes + "\n" + bare_statements + functions;

        const data = {
            tag: "div",
            content: [
                { tag: "h1", content: "Code" },
                { tag: "pre", content: code }
            ]
        };

        while (codeview.lastChild)
        {
            codeview.removeChild(codeview.lastChild);
        }

        codeview.appendChild(create_element(data));

        return false;
    };

    codeview_button.addEventListener("click", start_generating_code );
};

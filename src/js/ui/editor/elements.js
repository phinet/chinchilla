import { theme as Theme } from "../theme";
import { create_element } from "../../util/create_element";
import { ui } from "../create_gui";
import state from "../../state";
import * as symbols from "../../symbols";

export const root = [];

Theme.set({
    block_background: "#333",
    block_inactive_background: "#222",
    block_highlight:  "#666",
    block_inactive_highlight:  "#444",
    block_foreground: "#fff",
    block_inactive_foreground: "#ccc",
});

// Generic element creation start

const create_generic_element = (parent, x, y, c, data, children) =>
{
    const content = create_element({ tag: "div.content", content: children });

    content.style.setProperty("--background", c[0]);
    content.style.setProperty("--highlight", c[1]);
    content.style.setProperty("--foreground", c[2]);

    content.style.setProperty("--inactive_background", c[3]);
    content.style.setProperty("--inactive_highlight", c[4]);
    content.style.setProperty("--inactive_foreground", c[5]);

    const container = create_element({
        tag: "div.element",
        content: content
    });

    content.data = data;
    container.data = data;

    data.content = content;
    data.container = container;

    data.parent = null;
    data.child = null;
    data.data = data;

    if (parent)
    {
        reparent_element(parent, data);
    }
    else
    {
        data.free = true;
        container.classList.add("free");

        container.style.top = y + "px";
        container.style.left = x + "px";
    }

    return data;
};

const create_block_element = (parent, x, y, c) =>
{
    const top = create_element({ tag: "div.block_top", events: { mousedown: drag_start } });
    const bottom = create_element({ tag: "div.block_bottom", events: { mousedown: drag_start } });
    const side = create_element({ tag: "div.block_side", events: { mousedown: drag_start } });

    const placeholder = create_element({ tag: "div.block_placeholder.element" });
    const body = create_element({ tag: "div.block_body.element", content: placeholder });

    top.drop_event = drag_over_block_top;
    placeholder.drop_event = drag_over_block_placeholder;
    body.drop_event = drag_over_block_body;
    side.drop_event = drag_over_block_side;
    bottom.drop_event = drag_over_block_bottom;

    const content = [ top, { tag: "br" }, body, { tag: "br" }, bottom, side ];

    const data = {
        top: top,
        body: {
            parent: false,
            content: body,
            container: body,
            child: null
        },
        side: side,
        bottom: bottom
    };

    placeholder.data = data.body;

    top.data = data;
    body.data = data;
    bottom.data = data;
    side.data = data;

    data.body.body = data.body;

    return create_generic_element(parent, x, y, c, data, content);
};

const create_inline_element = (parent, x, y, c) =>
{
    const content = create_element({ tag: "div", class: "inline_element", events: { mousedown: drag_start } });

    content.drop_event = drag_over_inline_element;

    const data = {
        content: content,
        top: content,
        inline: true
    };

    content.data = data;

    return create_generic_element(parent, x, y, c, data, content);
};

const input_text_event = (e) =>
{
    const target = e.currentTarget;

    requestAnimationFrame(() =>
    {
        target.style.width = target.value.length + "ch";
        if (target.data.vararg_check) target.data.vararg_check();
    });
};

const input_mouse_event = (e) =>
{
    e.stopPropagation();

    return false;
};

const create_field = (element, data) =>
{
    const placeholder = data && data.tag || "";

    const input = create_element({
        tag: "input",
        type: "text",
        size: placeholder.length || 1,
        placeholder: placeholder,
        events: {
            change: input_text_event,
            paste: input_text_event,
            keydown: input_text_event,
            mousedown: input_mouse_event
        },
        disabled: element.data.disabled
    });

    input.data = element.data;

    const field = create_element({
        tag: "div.field",
        events: {
            mouseenter: field_enter,
            mouseleave: field_exit
        },
        content: input
    });

    field.content = field.container = field;
    field.drop_event = drag_over_field;
    field.is_field = true;
    field.data = element.data;

    return field;
};

const add_text = (element, data) =>
{
    data = data.slice(); // clone

    for (let i = 0; i < data.length; i++)
    {
        if (typeof data[i] === "string")
        {
            data[i] = create_element({
                tag: "span",
                content: data[i]
            });
        }
        else
        {
            data[i] = create_field(element, data[i]);
        }
    }

    element.appendChild(create_element({ tag:"div.text", content: data }));
};

const vararg_check = (data) =>
{
    if (data.disabled) return;

    try
    {
        const nodes = data.top.getElementsByClassName("text")[0].childNodes;

        let dots = nodes[nodes.length - 2];
        let last = nodes[nodes.length - 4];

        const add_field = () =>
        {
            dots.parentElement.insertBefore(create_field(data.top), dots);
            dots.parentElement.insertBefore(create_element({ tag: "span", content: "," }), dots);
        };

        if (!last || last.nodeName !== "DIV" || !last.lastChild)
        {
            add_field();

            return;
        }

        last = last.lastChild;

        if (last.nodeName === "INPUT" && last.value.length === 0)
        {
            // do nothing
        }
        else
        {
            add_field();
        }
    }
    catch (e)
    {
        console.error("A problem with vararg creation", e);
    }
};

const create_block = (type, disabled) =>
{
    const specs = (typeof(type) === "string")? blocks.types[type]: type;

    const builder = specs.builder;
    const supertype = specs.supertype;
    const colours = specs.colours;
    const top = specs.top;
    const bottom = specs.bottom;
    const disable_top = specs.disable_top;
    const disable_bottom = specs.disable_bottom;
    const varargs = specs.varargs;

    const theme = (n) => [
        Theme.get(n + "_background") || Theme.get("block_background"),
        Theme.get(n + "_highlight") || Theme.get("block_highlight"),
        Theme.get(n + "_foreground") || Theme.get("block_foreground"),

        Theme.get(n + "_inactive_background") || Theme.get("block_inactive_background"),
        Theme.get(n + "_inactive_highlight") || Theme.get("block_inactive_highlight"),
        Theme.get(n + "_inactive_foreground") || Theme.get("block_inactive_foreground")
    ];

    const create = () =>
    {
        const c = theme(colours);
        const e = builder(null, 0, 0, c);

        e.supertype = supertype;
        e.typestring = type;
        e.disabled = disabled;
        e.name = type.name;
        e.userdefined = type.userdefined;
        if (varargs) e.vararg_check = () => void vararg_check(e);
        if (top) add_text(e.top, top);
        if (bottom) add_text(e.bottom, bottom);
        if (varargs) e.vararg_check();
        if (disable_top) e.content.classList.add("disable_top");
        if (disable_bottom) e.content.classList.add("disable_bottom");

        return e;
    };

    return create();
};

// manage what supertypes have been registered
const pallete_supertypes = {};

const create_pallete_block = (type) =>
{
    const e = create_block(type, true);

    e.palleted = true;

    if (!pallete_supertypes[e.supertype])
    {
        const title = create_element({ tag: "h2",
            content: e.supertype,
            events: {
                click: (e) =>
                {
                    e.currentTarget.parentElement.classList.toggle("closed");
                }
            } });

        const container = create_element({ tag: "div.supertype.closed", content: title });

        pallete_supertypes[e.supertype] = container;

        const order = Object.keys(pallete_supertypes).sort();
        const index = order.indexOf(e.supertype);

        if (index >= order.length - 1)
        {
            ui.pallete.appendChild(container);
        }
        else
        {
            const next = pallete_supertypes[order[index + 1]];

            ui.pallete.insertBefore(container, next);
        }
    }

    pallete_supertypes[e.supertype].appendChild(e.container);

    if (e.userdefined)
    {
        requestAnimationFrame(() =>
        {
            const new_element = create_block(e.typestring);

            symbols.functions[e.name].definition = new_element;

            reparent_element(null, new_element);

            e.container.style.display = "none";
        });
    }

    return e.container;
};

// Generic element creation end

// Element hierarchy helpers start

const get_last_child = (e) =>
{
    while (e.child)
    {
        e = e.child;
    }

    return e;
};

const field_reparented = (child) =>
{
    try
    {
        if (child.parent.data.vararg_check) child.parent.data.vararg_check();
    }
    catch (e)
    {}
};

const add_to_root = (e) =>
{
    root.push(e);

    // console.log("added to root", e);
};

const remove_from_root = (e) =>
{
    let i;

    while ((i = root.indexOf(e)) >= 0)
    {
        root.splice(i, 1);
    }

    // console.log("removed from root", e);
};

const reparent_element = (parent, element) =>
{
    element.container.style.left = null;
    element.container.style.top = null;

    if (element.in_field) field_reparented(element);

    remove_from_root(element);

    if (!parent)
    {
        if (element.parent)
        {
            element.parent.container.removeChild(element.container);
            element.parent.child = null;
            element.parent = null;
        }

        element.in_field = false;

        ui.editor.appendChild(element.container);

        add_to_root(element);
    }
    else
    {
        const old = parent.child;

        parent.child = element;
        element.parent = parent;

        if (old)
        {
            const last = get_last_child(element);

            old.parent = last;
            last.child = old;
            parent.container.removeChild(old.container);
            last.container.appendChild(old.container);
        }
        else
        {
            remove_from_root(element);
        }

        element.in_field = parent.is_field;

        parent.container.appendChild(element.container);
    }

    if (element.in_field) field_reparented(element);
};

// Element hierarchy helpers end

// Drag listeners start (move element on canvas)

const rem = parseFloat(getComputedStyle(document.documentElement).fontSize);

const drop_threshold = rem / 2; // height from top of element to consider it bottom

let dragging_element, drop_target, drop_position, delete_dragging_element, hovering_field;

let z_index = 0;

const change_drop_target = (e) =>
{
    const new_target = e && e.drop_target;
    const new_position = e && e.drop_position;

    if (!new_target && !drop_target) return;

    if (new_target === drop_target && new_position === drop_position) return;

    if (drop_target)
    {
        drop_target.content.classList.remove("above");
        drop_target.content.classList.remove("below");
    }

    if (new_target)
    {
        new_target.content.classList.add(new_position);
    }

    drop_target = new_target;
    drop_position = new_position;
};

let last_x, last_y;

const distance = (e) =>
{
    const d = drop_threshold / 2;

    const r = last_x === undefined
                || last_y === undefined
                || Math.abs(e.clientX - last_x) > d
                || Math.abs(e.clientY - last_y) > d;

    if (r)
    {
        last_x = e.clientX;
        last_y = e.clientY;
    }

    return r;
};

const drag_start = (e) =>
{
    e.stopPropagation();

    if (state.running) return;

    if (e.button !== 0) return;

    last_x = e.clientX;
    last_y = e.clientY;

    dragging_element = e.currentTarget.data;

    ui.pallete.classList.remove("delete");
    delete_dragging_element = false;

    const rect = dragging_element.container.getBoundingClientRect();

    const mouse = {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };

    if (dragging_element.palleted)
    {
        const new_element = create_block(dragging_element.typestring);

        if (dragging_element.userdefined)
        {
            symbols.functions[dragging_element.name].definition = new_element;

            // dragging_element.container.style.display = "none";
        }

        new_element.free = false;
        new_element.fresh = true;
        delete_dragging_element = true;

        new_element.container.style.visibility = "hidden";

        ui.editor.appendChild(new_element.container);

        dragging_element = new_element;
    }

    dragging_element.container.classList.add("floating");

    dragging_element.container.style.zIndex = ++z_index;

    dragging_element.mouse = mouse;
};

const noop = () =>
{};

const create_hover_event = () =>
{
    let e;

    const r = dragging_element.container.getBoundingClientRect();

    if (parseFloat(dragging_element.container.style.left) < 0) return;

    const get_data = (x, y) =>
    {
        let target = document.elementFromPoint(x, y);

        while (target && !target.drop_event)
        {
            target = target.parentElement;
        }

        if (target)
        {
            return {
                currentTarget: target,
                clientX: x,
                clientY: y
            };
        }
    };

    if (hovering_field)
    {
        e = {
            currentTarget: hovering_field,
            clientX: 0,
            clientY: 0
        };

        if (e.currentTarget.drop_event(e)) return;
    }

    e = get_data(r.left, r.top - 1);
    if (e) return e.currentTarget.drop_event(e);

    for (let i = r.top; i <= r.bottom; i += drop_threshold / 2)
    {
        e = get_data(r.left - 1, i);
        if (e) return e.currentTarget.drop_event(e);
    }

    e = get_data(r.left, r.bottom + 1);
    if (e) return e.currentTarget.drop_event(e);

    change_drop_target(null);
};

const drag_move = (e) =>
{
    e.stopPropagation();

    if (!dragging_element) return;

    if (!dragging_element.free && distance(e))
    {
        if (dragging_element.fresh)
        {
            dragging_element.fresh = false;
            dragging_element.container.style.visibility = "visible";
        }

        last_x = last_y = undefined;
        dragging_element.free = true;

        dragging_element.container.classList.add("free");
        reparent_element(null, dragging_element);
    }

    if (!dragging_element.free) return;

    const rect = ui.editor.getBoundingClientRect();

    const client_x = e.clientX - dragging_element.mouse.x;
    const client_y = e.clientY - dragging_element.mouse.y;

    const x = client_x - rect.left + ui.editor.scrollLeft;
    const y = client_y - rect.top + ui.editor.scrollTop;

    dragging_element.container.style.left = x + "px";
    dragging_element.container.style.top = y + "px";

    if (distance(e)) create_hover_event();
};

const larger_element = (a, b) => (a.offsetWidth * a.offsetHeight >= b.offsetWidth * b.offsetHeight)? a: b;

const drag_end = (e) =>
{
    if (!dragging_element) return;

    if (dragging_element.fresh)
    {
        dragging_element.container.parentElement.removeChild(dragging_element.container);

        dragging_element = null;

        return;
    }

    if (dragging_element.userdefined)
    {
        symbols.functions[dragging_element.name].def.style.display = "none";
    }

    delete dragging_element.mouse;

    dragging_element.container.classList.remove("floating");

    const insert = (target) =>
    {
        delete dragging_element.free;

        dragging_element.container.classList.remove("free");

        reparent_element(target, dragging_element);
    };

    if (drop_target)
    {
        if (drop_position === "above")
        {
            if (drop_target.parent)
            {
                // insert between
                insert(drop_target.parent);
            }
            else if (drop_target.parent === false)
            {
                // insert at top of nested thing
                insert(drop_target);
            }
            else
            {
                // insert above
                const larger = larger_element(dragging_element.container, drop_target.container);

                let top = larger.style.top;

                if (larger !== dragging_element.container)
                {
                    top = Math.max(0, parseFloat(top) - dragging_element.container.offsetHeight) + "px";
                }

                dragging_element.container.style.top = top;
                dragging_element.container.style.left = larger.style.left;

                const last = get_last_child(dragging_element);

                dragging_element = drop_target;

                insert(last);
            }
        }
        else
        {
            // insert below
            insert(drop_target);
        }

        change_drop_target(null);
    }
    else if (delete_dragging_element)
    {
        dragging_element.container.parentElement.removeChild(dragging_element.container);

        ui.pallete.classList.remove("delete");
        delete_dragging_element = false;

        if (dragging_element.name)
        {
            const func = symbols.functions[dragging_element.name];

            if (func && dragging_element === func.definition)
            {
                func.definition = null;
                func.def.style.display = null;
            }
        }

        remove_from_root(dragging_element);

        dragging_element = null;
    }
    else
    {
        const s = dragging_element.container.style;

        if (parseFloat(s.left) < 0)
        {
            s.left = 0;
        }

        if (parseFloat(s.top) < 0)
        {
            s.top = 0;
        }
    }

    dragging_element = undefined;
};

// TODO disable decalre nesting -----------------------------------------------------------------------------------------------------

const drag_over_element = (e) =>
{
    if (!dragging_element) return;

    let target = e.currentTarget;
    let position;

    let neighbor_up, neighbor_down;
    let disable_top, disable_bottom;

    const x = e.clientX - target.getBoundingClientRect().left;
    const y = e.clientY - target.getBoundingClientRect().top;

    const check_above = () =>
    {
        return !!target.parent || target.parent === false;
    };

    const check_below = () =>
    {
        return !!target.child;
    };

    const above = () =>
    {
        target = e.currentTarget.data;
        position = "above";

        neighbor_up = check_above();
        neighbor_down = true;
    };

    const below = () =>
    {
        target = e.currentTarget.data;
        position = "below";

        neighbor_up = true;
        neighbor_down = check_below();
    };

    const body = () =>
    {
        target = e.currentTarget.data.body;
        position = "above";

        neighbor_up = true;
        neighbor_down = check_below();
    };

    const body_below = () =>
    {
        target = e.currentTarget.data.body;

        while (target.child)
        {
            target = target.child;
        }

        position = (target === e.currentTarget.data.body)? "above": "below";

        neighbor_up = true;
        neighbor_down = check_below();
    };

    const field = () =>
    {
        target = e.currentTarget;

        position = "below";

        neighbor_up = true;
        neighbor_down = true;

        return dragging_element.inline && !dragging_element.child && !target.child;
    };

    const none = () => target = position = undefined;

    switch (e.drag_type)
    {
        case "block_placeholder":
            body();

            break;
        case "block_body":
            if (x < rem)
            {
                below();
            }
            else
            {
                none();
            }

            break;
        case "block_top":
            if (x < rem || y < drop_threshold)
            {
                above();
            }
            else
            {
                body();
            }

            break;
        case "block_bottom":
            if (x < rem || y >= drop_threshold)
            {
                below();
            }
            else
            {
                body_below();
            }

            break;
        case "block_side":
        case "inline":
            if (y < drop_threshold)
            {
                above();
            }
            else
            {
                below();
            }

            break;
        case "field":
            if (!field()) return;

            break;
        default:
            console.log("Unknown drop type", e.drag_type);

            return;
    }

    if (!target || target.in_field) return;

    disable_top = dragging_element.content.classList.contains("disable_top");
    disable_bottom = dragging_element;

    while (disable_bottom.child)
    {
        disable_bottom = disable_bottom.child;
    }

    disable_bottom = disable_bottom.content.classList.contains("disable_bottom");

    if (neighbor_up && disable_top) return;
    if (neighbor_down && disable_bottom) return;

    if (position === "above" && target.content.classList.contains("disable_top")) return;
    if (position === "below" && target.content.classList.contains("disable_bottom")) return;

    if (target.content.classList.contains("field") && (disable_top || disable_bottom)) return;

    e.drop_target = target;
    e.drop_position = position;

    change_drop_target(e);

    return true;
};

const drag_over_inline_element = (e) =>
{
    e.drag_type = "inline";

    return drag_over_element(e);
};

const drag_over_block_top = (e) =>
{
    e.drag_type = "block_top";

    return drag_over_element(e);
};

const drag_over_block_placeholder = (e) =>
{
    e.drag_type = "block_placeholder";

    return drag_over_element(e);
};

const drag_over_block_body = (e) =>
{
    e.drag_type = "block_body";

    return drag_over_element(e);
};

const drag_over_block_bottom = (e) =>
{
    e.drag_type = "block_bottom";

    return drag_over_element(e);
};

const drag_over_block_side = (e) =>
{
    e.drag_type = "block_side";

    return drag_over_element(e);
};

const drag_over_field = (e) =>
{
    e.drag_type = "field";

    return drag_over_element(e);
};

const field_enter = (e) =>
{
    if (dragging_element) hovering_field = e.currentTarget;
};

const field_exit = (e) =>
{
    hovering_field = null;
};

const pallete_enter = (e) =>
{
    if (!dragging_element) return;

    ui.pallete.classList.add("delete");
    delete_dragging_element = true;
};

const pallete_exit = (e) =>
{
    if (!dragging_element) return;

    ui.pallete.classList.remove("delete");
    delete_dragging_element = false;
};

const types = {
    "if": {
        builder: create_block_element,
        colours: "control",
        supertype: "Flow Control",
        top: [ "if (", { tag: "condition"}, ") {" ],
        bottom: [ "}" ]
    },
    "else if": {
        builder: create_block_element,
        colours: "control",
        supertype: "Flow Control",
        top: [ "else if (", { tag: "condition"}, ") {" ],
        bottom: [ "}" ]
    },
    "else": {
        builder: create_block_element,
        colours: "control",
        supertype: "Flow Control",
        top: [ "else {" ],
        bottom: [ "}" ]
    },
    "for loop": {
        builder: create_block_element,
        colours: "control",
        supertype: "Loops",
        top: [ "for (", { tag: "initalize"}, ";", { tag: "condition"}, ";", { tag: "update"}, ") {" ],
        bottom: [ "}" ]
    },
    "while loop": {
        builder: create_block_element,
        colours: "control",
        supertype: "Loops",
        top: [ "while (", { tag: "condition"}, ") {" ],
        bottom: [ "}" ]
    },
    "do while loop": {
        builder: create_block_element,
        colours: "control",
        supertype: "Loops",
        top: [ "do {" ],
        bottom: [ "} while (", { tag: "condition"}, ");" ]
    },
    "plus": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Math",
        top: [ 0, "+", 0 ]
    },
    "minus": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Math",
        top: [ 0, "-", 0 ]
    },
    "times": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Math",
        top: [ 0, "*", 0 ]
    },
    "divide": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Math",
        top: [ 0, "/", 0 ]
    },
    "remainder": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Math",
        top: [ 0, "%", 0 ]
    },
    "return": {
        builder: create_inline_element,
        colours: "control",
        supertype: "Flow Control",
        top: [ "return", 0 ],
        disable_bottom: true
    },
    "variable declaration and assignment": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variables",
        top: [ { tag: "type", type: "string"}, { tag: "name", type: "string"}, "=", 0]
    },
    "variable declaration": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variables",
        top: [ { tag: "type", type: "string"}, { tag: "name", type: "string"}]
    },
    "variable assignment": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variables",
        top: [ { tag: "name", type: "string"}, "=", 0]
    },
    "variable access": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variables",
        top: [ { tag: "name", type: "string"}]
    },
    "post increment": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variable Modification",
        top: [ { tag: "name", type: "string"}, "++", ]
    },
    "post decrement": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variable Modification",
        top: [ { tag: "name", type: "string"}, "--"]
    },
    "pre increment": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variable Modification",
        top: [ "++", { tag: "name", type: "string"} ]
    },
    "pre decrement": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variable Modification",
        top: [ "--", { tag: "name", type: "string"} ]
    },
    "plus assign": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variable Modification",
        top: [ { tag: "name", type: "string"}, "+=", 0 ]
    },
    "minus assign": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variable Modification",
        top: [ { tag: "name", type: "string"}, "-=", 0 ]
    },
    "times assign": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variable Modification",
        top: [ { tag: "name", type: "string"}, "*=", 0 ]
    },
    "divide assign": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variable Modification",
        top: [ { tag: "name", type: "string"}, "/=", 0 ]
    },
    "remainder assign": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variable Modification",
        top: [ { tag: "name", type: "string"}, "%=", 0 ]
    },
    "equivalent": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Comparison",
        top: [ 0, "==", 0]
    },
    "not equivalent": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Comparison",
        top: [ 0, "!=", 0]
    },
    "less than or equivalent": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Comparison",
        top: [ 0, "<=", 0]
    },
    "greater than or equivalent": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Comparison",
        top: [ 0, ">=", 0]
    },
    "less than": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Comparison",
        top: [ 0, "<", 0]
    },
    "greater than": {
        builder: create_inline_element,
        colours: "expression",
        supertype: "Comparison",
        top: [ 0, ">", 0]
    },
    "dereference": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variables",
        top: [ "*", { tag: "address", type: "string"}]
    },
    "address of": {
        builder: create_inline_element,
        colours: "variable",
        supertype: "Variables",
        top: [ "&", { tag: "name", type: "string"}]
    }
};

export const blocks = {
    create_inline_element,
    create_block_element,
    create_block,
    create_pallete_block,
    types,
};

export const init = () =>
{
    window.addEventListener("mousemove", drag_move);
    window.addEventListener("mouseup", drag_end);

    ui.pallete.addEventListener("mouseenter", pallete_enter);
    ui.pallete.addEventListener("mouseleave", pallete_exit);
};

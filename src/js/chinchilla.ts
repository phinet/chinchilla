import { theme } from "./ui/theme";
import * as GUI from "./ui/create_gui";
import * as Elements from "./ui/editor/elements";
import * as Blocks from "./language/blocks";
import * as Console from "./ui/console/io";
import * as Code from "./ui/editor/c_code";

export const chinchilla = {};

theme.set({
    theme:          "#919191", // grey
    theme_light:    "#bcbcbc",
    theme_lighter:  "#d3d3d3",
    theme_lightest: "#ffffff",
    theme_dark:     "#4c4c4c",
    theme_darker:   "#3a3a3a",
    theme_darkest:  "#000000",

    theme_a:          "#1097FF", // blue
    theme_a_light:    "#79C5FF",
    theme_a_lighter:  "#AADAFF",
    theme_a_lightest: "#F4FAFF",
    theme_a_dark:     "#004F8C",
    theme_a_darker:   "#003D6B",
    theme_a_darkest:  "#001627",

    theme_b:          "#FFBF00", // yellow
    theme_b_light:    "#FFDB70",
    theme_b_lighter:  "#FFE8A4",
    theme_b_lightest: "#FFFCF3",
    theme_b_dark:     "#D9A300",
    theme_b_darker:   "#A77D00",
    theme_b_darkest:  "#3C2D00",

    theme_c:          "#FF4900", // red
    theme_c_light:    "#FF9970",
    theme_c_lighter:  "#FFBEA4",
    theme_c_lightest: "#FFF6F3",
    theme_c_dark:     "#D93E00",
    theme_c_darker:   "#A72F00",
    theme_c_darkest:  "#3C1100",

    theme_d:          "#2626FF", // dark blue
    theme_d_light:    "#8585FF",
    theme_d_lighter:  "#B1B1FF",
    theme_d_lightest: "#F5F5FF",
    theme_d_dark:     "#000098",
    theme_d_darker:   "#000075",
    theme_d_darkest:  "#00002A",

    theme_e:          "#00FF92", // green
    theme_e_light:    "#70FFC2",
    theme_e_lighter:  "#A4FFD8",
    theme_e_lightest: "#F3FFFA",
    theme_e_dark:     "#009555",
    theme_e_darker:   "#007241",
    theme_e_darkest:  "#002918",
});

theme.set({
    control_background: theme.get("theme_e"),
    control_inactive_background: theme.get("theme_e_dark"),
    control_highlight:  theme.get("theme_e_dark"),
    control_inactive_highlight:  theme.get("theme_e_darker"),
    control_foreground: theme.get("theme_lightest"),
    control_inactive_foreground: theme.get("theme_e_light"),

    function_background: theme.get("theme_a"),
    function_inactive_background: theme.get("theme_a_dark"),
    function_highlight:  theme.get("theme_a_dark"),
    function_inactive_highlight:  theme.get("theme_a_darker"),
    function_foreground: theme.get("theme_lightest"),
    function_inactive_foreground: theme.get("theme_a_light"),

    expression_background: theme.get("theme_c"),
    expression_inactive_background: theme.get("theme_c_dark"),
    expression_highlight:  theme.get("theme_c_dark"),
    expression_inactive_highlight:  theme.get("theme_c_darker"),
    expression_foreground: theme.get("theme_lightest"),
    expression_inactive_foreground: theme.get("theme_c_light"),

    variable_background: theme.get("theme_b"),
    variable_inactive_background: theme.get("theme_b_dark"),
    variable_highlight:  theme.get("theme_b_dark"),
    variable_inactive_highlight:  theme.get("theme_b_darker"),
    variable_foreground: theme.get("theme_lightest"),
    variable_inactive_foreground: theme.get("theme_b_light"),

    standard_library_background: theme.get("theme_d"),
    standard_library_inactive_background: theme.get("theme_d_dark"),
    standard_library_highlight:  theme.get("theme_d_dark"),
    standard_library_inactive_highlight:  theme.get("theme_d_darker"),
    standard_library_foreground: theme.get("theme_lightest"),
    standard_library_inactive_foreground: theme.get("theme_d_light"),
});

GUI.build();
Blocks.init();
Elements.init();
Console.start();
Code.start();

Object.keys(Elements.blocks.types).forEach(Elements.blocks.create_pallete_block);

import { create_element } from "../util/create_element";
import { blocks, root } from "../ui/editor/elements";
import { ui } from "../ui/create_gui";
import { io } from "../ui/console/io";
import * as Literals from "./literals";
import state from "../state";
import * as symbols from "../symbols";

export const memory = {};

export const stack = [];

let delay = 250;

export const setDelay = (newDelay) =>
{
    delay = newDelay;
};

window.addEventListener("error", (e) =>
{
    if (state.running)
    {
        console.log(e);

        document.body.classList.remove("nohighlight");

        ui.error.textContent = e.message;
        print_variables();
    }
});

/**
 * Entrypoint. Resets state, calls main
 */

export const run = () =>
{
    console.clear();

    console.log("Starting");

    console.log(delay);

    document.body.classList.remove("nohighlight");
    if (delay === 0) document.body.classList.add("nohighlight");

    memory.heap = {};
    memory.global = collect_global_variables();
    memory.inaccessible = [];
    memory.accessible = null;

    stack.length = 0;

    const main = symbols.functions.main;

    const exit = (value) =>
    {
        console.log("Done", value);

        document.body.classList.remove("running");
        document.body.classList.remove("paused");
        document.body.classList.remove("nohighlight");

        state.running = false;

        print_variables();

        io.clearBuffer();
    };

    ui.error.textContent = "";
    ui.memory.textContent = "";

    if (main.args.length === 2)
    {
        call_function(exit, main.definition, [ 0, 0 ]);
    }
    else
    {
        call_function(exit, main.definition, []);
    }
};

/**
 * Evaluates any code that is orphaned from functiuon. ie creates global variables
 */

const collect_global_variables = () =>
{
    console.log("Collecting globals");

    const data = {
        name: "global scope",
        names: {}
    };

    root.forEach((e) =>
    {
        if (e === symbols.functions.main.definition)
        {
            console.log("main. skipping");

            return;
        }

        console.log(e);
    });

    return data;
};

const get_variable_data = () =>
{
    const get_data = (scope) =>
    {
        const primary = {};
        const secondary = [];
        const addresses = [];

        const data = [primary, secondary, addresses];

        if (!scope) return data;

        do
        {
            const temp = {};

            for (const name in scope.names)
            {
                const address = scope.names[name].address;

                addresses.push(address);

                temp[name] = memory.heap[address].value;

                if (!primary.hasOwnProperty(name))
                {
                    primary[name] = temp[name];
                }
            }

            if (Object.keys(temp).length > 0)
            {
                secondary.push(temp);
            }
        } while (scope = scope.next);

        return data;
    };

    const active_data = get_data(memory.accessible);

    const accessible = active_data[0];
    const active = active_data[1];
    const inactive = [];

    const stack_addresses = active_data[2];
    const heap = {};

    for (const level of memory.inaccessible)
    {
        if (!level) continue;

        const temp = get_data(level);

        if (temp[1].length > 0)
        {
            inactive.push(temp[1]);
        }

        for (const address of temp[2])
        {
            stack_addresses.push(address);
        }
    }

    console.log(stack_addresses);

    for (const address in memory.heap)
    {
        heap[address] = memory.heap[address].value;
    }

    console.log(heap);

    for (const address of stack_addresses)
    {
        delete heap[address];
    }

    console.log(heap);

    return {
        accessible: accessible,
        active: active,
        inactive: inactive,
        heap: heap,
    };
};

const print_variables = () =>
{
    const data = get_variable_data();

    let accessible = [];
    let active = [];
    let inactive = [];
    let heap = [];

    for (const k in data.accessible)
    {
        accessible.push(k + ": " + data.accessible[k]);
    }

    for (const l of data.active)
    {
        let temp = [];

        for (const k in l)
        {
            temp.push(k + ": " + l[k]);
        }

        active.push("[ " + temp.join(", ") + " ]");
    }

    for (const f of data.inactive)
    {
        let t = [];

        for (const l of f)
        {
            let temp = [];

            for (const k in l)
            {
                temp.push(k + ": " + l[k]);
            }

            t.push("    [ " + temp.join(", ") + " ]");
        }

        inactive.push("[\n" + t.join(",\n") + "\n]");
    }

    for (const k in data.heap)
    {
        heap.push(data.heap[k] + "");
    }

    accessible = accessible.join(", ");
    active = active.join(",\n");
    inactive = inactive.join(", ");
    heap = heap.join(", ");

    const element = create_element({
        tag: "div",
        content: [
            { tag:  "h1", content: "Accessible data: " },
            { tag: "pre", content: accessible },
            { tag:  "h1", content: "Active data:" },
            { tag: "pre", content: active },
            { tag:  "h1", content: "Inactive data:" },
            { tag: "pre", content: inactive },
            { tag:  "h1", content: "Heap:" },
            { tag: "pre", content: heap },
        ]
    });

    while (ui.memory.lastChild)
    {
        ui.memory.removeChild(ui.memory.lastChild);
    }

    ui.memory.appendChild(element);
};

/**
 * Increases block level scope by 1
 */

const push_block_scope = (name) =>
{
    const temp = {
        name: name,
        names: {},
        next: memory.accessible
    };

    memory.accessible = temp;
};

/**
 * Decrease block level scope by 1
 */

const pop_block_scope = (name) =>
{
    const names = memory.accessible.names;
    const keys = Object.keys(names);

    for (const name of keys)
    {
        delete memory.heap[names[name].address];
        delete names[name];
    }

    memory.accessible = memory.accessible.next;
};

/**
 * Scope reset when function is called. Only global scope is visible after call.
 * Calls block level Internally
 */

const push_function_scope = (name) =>
{
    memory.inaccessible.push(memory.accessible);

    memory.accessible = memory.global;

    push_block_scope(name);
};

/**
 * Scope reset when function is called. Only global scope is visible after call.
 * Calls block level Internally
 */

const pop_function_scope = (name) =>
{
    pop_block_scope();

    memory.accessible = memory.inaccessible.pop();
};

/**
 * Allocates memory on 'heap'
 * Random address is returned. (Address has no collisions with other allocated memory)
 */

const create_memory_allocation = (size) =>
{
    let address;

    const old = Object.keys(memory.heap).map((n) => parseInt(n));

    const validate = () =>
    {
        for (let i = 0; i < old.length; i++)
        {
            const old_address = old[i];

            if (old_address > address + size) return true; // good, past all relevant things

            if (old_address >= address) return false; // old alloc in new

            if (old_address + memory.heap[old_address].size >= address) return false; // new alloc in old
        }

        return true;
    };

    do
    {
        address = Math.floor(Math.random() * (Number.MAX_SAFE_INTEGER - size));
    } while (!validate());

    memory.heap[address] = {size: size};

    return address;
};

/**
 * Looks up variable. returns variable or undefined
 */

const lookup_variable = (name) =>
{
    let scope = memory.accessible;

    do
    {
        if (scope.names[name]) return scope.names[name];

        scope = scope.next;
    } while (scope);
};

/**
 * Changes value of previously defined variable
 */

const define_variable = (name, value) =>
{
    const variable = lookup_variable(name);

    memory.heap[variable.address].value = value;
};

/**
 * Return live representation of variable
 */
export const access_variable = (name) =>
{
    if (typeof name === "string")
    {
        const info = lookup_variable(name);

        return memory.heap[info.address];
    }
    else
    {
        console.log(name);
        console.log(memory.heap);

        return memory.heap[name];
    }
};

export const memory_alloc = (size) =>
{
    const address = create_memory_allocation(size);

    memory.heap[address].on_heap = true;

    return address;
};

export const memory_free = (address) =>
{
    const data = memory.heap[address];

    if (!data)
    {
        throw "Memory not allocated";
    }

    if (!data.on_heap)
    {
        throw "Not a valid heap address";
    }

    delete memory.heap[address];
};

/**
 * Declares variable, adding it to proper location in scope
 * Optionally defines variable
 */

const declare_variable = (name, type, value) =>
{
    if (memory.accessible.names[name])
    {
        throw "Variable redefinition";
    }

    const size = 8;

    memory.accessible.names[name] = {
        size: size,
        type: type,
        address: create_memory_allocation(size)
    };

    if (typeof value !== "undefined") define_variable(name, value);
};

/**
 * Function call
 * Not done
 */

const call_function = (callback, block, args) =>
{
    console.log("Function call - " + block.name);

    access_block(block);

    push_function_scope("functions - " + block.name);

    stack.push({
        type: "function call",

        callback: (result) =>
        {
            pop_function_scope();

            callback(result);
        }
    });

    const def = symbols.functions[block.name];

    if (args.length < def.args.length)
    {
        // invalid
    }
    else
    {
        const extra_args = args.splice(def.args.length);

        if (extra_args.length > 0)
        {
            if (!def.args.varargs)
            {
                //invalid
            }

            memory.accessible.varargs = extra_args;
        }

        for (let i = 0; i < def.args.length; i++)
        {
            declare_variable(def.args[i].name, def.args[i].type, args[i]);
        }
    }

    stack.push(block.body.child);

    console.log(stack);

    setTimeout(step, delay);

    // stack.push(definition.definition.body.child);

    // step();
};

export const get_fields = (block, definition) =>
{
    const array = [...block.data.container.getElementsByClassName("field")];

    const filtered = array.filter(f => f.data === block);

    const fields = filtered.map(f =>
    {
        let data = f.lastChild;

        if (data.tagName === "INPUT")
        {
            data = data.value.trim();

            if (data.length === 0) data = undefined;
        }

        return data;
    });

    if (definition && definition.args.varargs)
    {
        while (fields.length > definition.args.length && fields[fields.length - 1] === undefined)
        {
            fields.pop();
        }
    }

    return fields;
};

const binary_expression = (callback, fields, operation) =>
{
    evaluate((result) =>
    {
        result = operation(result[0], result[1]);

        callback(result);
    }, ...fields);
};

const increment_expression = (callback, fields, before, after) =>
{
    const name = Literals.identifier(fields[0]);

    const variable = access_variable(name);

    const to_return = variable.value + before;

    define_variable(name, to_return + after);

    callback(to_return);
};

const variable_assignment = (callback, fields, operation) =>
{
    const name = Literals.identifier(fields[0]);
    const variable = access_variable(name);

    evaluate((results) =>
    {
        const value = operation(variable.value, results[0]);

        define_variable(name, value);

        callback(value);
    }, fields[1]);
};

/**
 * expression implementations
 */
const expressions = {
    "plus":      (c, f) => void binary_expression(c, f, (a, b) => a + b),
    "minus":     (c, f) => void binary_expression(c, f, (a, b) => a - b),
    "times":     (c, f) => void binary_expression(c, f, (a, b) => a * b),
    "divide":    (c, f) => void binary_expression(c, f, (a, b) => a / b),
    "remainder": (c, f) => void binary_expression(c, f, (a, b) => a % b),

    "variable access": (callback, f) =>
    {
        const name = Literals.identifier(f[0]);

        const value = access_variable(name).value;

        callback(value);
    },

    "pre increment":  (c, f) => void increment_expression(c, f, 1, 0),
    "pre decrement":  (c, f) => void increment_expression(c, f, -1, 0),
    "post increment": (c, f) => void increment_expression(c, f, 0, 1),
    "post decrement": (c, f) => void increment_expression(c, f, 0, -1),

    "equivalent":                 (c, f) => void binary_expression(c, f, (a, b) => (a == b)? 1: 0),
    "not equivalent":             (c, f) => void binary_expression(c, f, (a, b) => (a != b)? 1: 0),
    "less than or equivalent":    (c, f) => void binary_expression(c, f, (a, b) => (a <= b)? 1: 0),
    "greater than or equivalent": (c, f) => void binary_expression(c, f, (a, b) => (a >= b)? 1: 0),
    "less than":                  (c, f) => void binary_expression(c, f, (a, b) => (a < b)? 1: 0),
    "greater than":               (c, f) => void binary_expression(c, f, (a, b) => (a > b)? 1: 0),

    "variable assignment": (c, f) => void variable_assignment(c, f, (v, n) => n),
    "plus assign":         (c, f) => void variable_assignment(c, f, (v, n) => v + n),
    "minus assign":        (c, f) => void variable_assignment(c, f, (v, n) => v - n),
    "times assign":        (c, f) => void variable_assignment(c, f, (v, n) => v * n),
    "divide assign":       (c, f) => void variable_assignment(c, f, (v, n) => v / n),
    "remainder assign":    (c, f) => void variable_assignment(c, f, (v, n) => v % n),

    "address of": (callback, f) =>
    {
        const name = Literals.identifier(f[0]);
        const address = lookup_variable(name).address;

        callback(address);
    },

    "dereference": (callback, f) =>
    {
        evaluate((results) =>
        {
            const address = results[0];

            const value = access_variable(address).value;

            callback(value);
        }, f[0]);
    },
};

for (var expression in expressions)
{
    if (!blocks.types[expression])
    {
        throw "No block for" + expression;
    }
}

let last_block;

const access_block = (block) =>
{
    if (last_block) last_block.classList.remove("active");

    console.log(block);

    if (!block) return;
    if (!block.data) return;
    if (!block.data.container) return;

    last_block = block.data.container;

    last_block.classList.add("active");
};

/**
 * Evaluate expression
 */
const evaluate = (callback, ...args) =>
{
    console.log("eval", args);

    const results = [];

    const step = () =>
    {
        const notify = (result) =>
        {
            results.push(result);

            if (results.length >= args.length)
            {
                console.log("results", results);

                callback(results);
            }
            else
            {
                setTimeout(step, delay);
            }
        };

        const data = args[results.length];

        if (typeof data === "string")
        {
            notify(Literals.literal(data));
        }
        else
        {
            if (!data) throw "Empty field";

            const block = data.data;

            access_block(block);

            const expression = expressions[block.typestring];
            const userdef = symbols.functions[block.typestring.name];
            const native = symbols.native[block.typestring.name];

            if (expression)
            {
                expression((result) =>
                {
                    access_block(block);

                    notify(result);
                }, get_fields(block));
            }
            else if (userdef)
            {
                evaluate((vars) =>
                {
                    call_function(notify, userdef.definition, vars);
                }, ...get_fields(block, userdef));
            }
            else if (native)
            {
                evaluate((vars) =>
                {
                    native.def(notify, ...vars);
                }, ...get_fields(block, native));
            }
            else
            {
                throw "Unknown expression type " + JSON.stringify(data);
            }
        }
    };

    if (args.length === 0)
    {
        setTimeout(() => callback([]), 0);
    }
    else
    {
        setTimeout(step, delay);
    }
};

/**
 * Runtime of program. evaluates each line
 */

const step = () =>
{
    const block = stack.pop();

    if (!block)
    {
        if (stack.length > 0) setTimeout(step, 0);

        return;
    }

    if (delay > 0) print_variables();

    //---------------------------Intercept  blocks---------------------//
    if (block.type === "function call")
    {
        setTimeout(block.callback, 0);

        return;
    }

    //Specal case for inner for loop logic
    if (block.type === "inner for loop")
    {
        //Increment from previous loop- discard result

        evaluate(() =>
        {
            access_block(block.parent);

            //Evaluate condition
            evaluate((results) =>
            {
                if (results[0]!= 0)
                { //If true- Add body to stack
                    console.log("Inner for loop");
                    stack.push(block);//Add inner for loop to stack
                    stack.push(block.inside);//Add inner code to run next pass
                }
                else
                { //add next element to stack
                    console.log("Exit for inner loop");
                    stack.push(block.after);
                }

                if (state.running) setTimeout(step, delay);//Attempt to make marcus happy
            }, block.condition);
        }, block.increment);

        return;
    }

    //---------------------------End of Intercept---------------------//
    let inside, after;

    const next_step = (result) =>
    {
        console.log("result: ", result);

        if (after)
        {
            stack.push(after);
        }

        if (inside)
        {
            stack.push(inside);
        }

        if (state.running) setTimeout(step, delay);
    };

    access_block(block);

    const fields = get_fields(block);

    console.log("line", block, fields);

    // test for statements
    switch (block.typestring)
    {
        case "return": {
            console.log("return", fields);

            let parent_function;

            for (let i = stack.length - 1; i >= 0; i--)
            {
                if (stack[i].type === "function call")
                {
                    parent_function = stack[i];

                    while (stack.pop() !== parent_function)
                        ;

                    break;
                }
            }

            if (!parent_function) throw "Cannot call return here";

            if (fields[0] === undefined)
            {
                setTimeout(parent_function.callback, delay);
            }
            else
            {
                evaluate((results) =>
                {
                    parent_function.callback(results[0]);
                }, fields[0]);
            }

            return;
        }

        case "variable declaration":
        case "variable declaration and assignment": {
            console.log("def");

            const type = Literals.typestring(fields[0]);
            const identifier = Literals.identifier(fields[1]);

            const declare = (value) =>
            {
                declare_variable(identifier, type, value);
                after = block.child;
                next_step();
            };

            if (fields.length === 3)
            {
                evaluate((results) => void declare(results[0]), fields[2]);
            }
            else
            {
                declare(undefined);
            }
        }

            break;

        case "if": {
            let conditions = [ fields[0] ];
            let outcomes = [ block.body.child ];
            let fallback;

            after = inside = undefined;

            let current = block;

            // collect if else blocks
            do
            {
                current = current.child;

                if (!current)
                {
                    after = undefined;
                }
                else if (current.typestring === "else if")
                {
                    conditions.push(get_fields(current)[0]);
                    outcomes.push(current.body.child);
                }
                else if (current.typestring === "else")
                {
                    fallback = current.body.child;
                    after = current.child;
                }
                else
                {
                    after = current;
                }
            } while (current && !after);

            const finished = () =>
            {
                if (!inside && fallback) inside = fallback;
                next_step();
            };

            const check_loop = (i) =>
            {
                if (i < conditions.length)
                {
                    evaluate((results) =>
                    {
                        if (results[0] == 0)
                        { // false
                            setTimeout(() => void check_loop(i + 1), delay);
                        }
                        else
                        { // true
                            inside = outcomes[i];
                            finished();
                        }
                    }, conditions[i]);
                }
                else
                {
                    finished();
                }
            };

            check_loop(0);
        }

            break;

        case "else if":

        case "else": {
            throw "dangling else";
        }

        case "for loop": {
            console.log("for");

            //Initialize the first field
            let initial = fields[0] || "1";

            evaluate(() =>
            {
                //Evaluate initial condition
                evaluate((results) =>
                {
                    if (results[0]!= 0)
                    { //If true- Add body to stack
                        console.log("Inside For loop");

                        let for_object ={
                            type:           "inner for loop",
                            parent: block,
                            condition:      fields[1] || "1",
                            increment:      fields[2] || "1",
                            inside:         block.body.child,
                            after:          block.child
                        };

                        console.log(for_object);
                        stack.push(for_object);
                        stack.push(block.body.child);
                    }
                    else
                    { //add next element to stack
                        console.log("Exit for loop");
                        stack.push(block.child);
                    }

                    if (state.running) setTimeout(step, delay);//Attempt to make marcus happy
                }, fields[1]||"1");
            }, initial);
        }

            break;

        case "while loop": {
            let condition = fields[0];
            //Evaluate the Condition

            evaluate((results) =>
            {
                if (results[0]!= 0)
                { //If true do body then push body to stack back on stack
                    //console.log("While true");
                    after =block;// Then itself execute second
                    inside =block.body.child;// Child to execute first
                }
                else
                { //if false do next
                    //console.log("While False");
                    after = block.child;
                }

                next_step();
            }, condition);
        }

            break;

        case "do while loop": {
            let condition = fields[0];

            inside = block.body.child;// Inner code to be executed every time

            evaluate((results) =>
            {
                if (results[0]!= 0)
                { //If eval true, Execute again with
                    console.log("Is true");
                    after = block;// Execute block again if true
                }
                else
                { //if false execute next block
                    console.log("Is False");
                    after = block.child;
                }

                next_step();
            }, condition);
        }

            break;


        default: {
            after = block.child;

            evaluate(next_step, block);
        }
    }
};

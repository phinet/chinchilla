const trigraphs = (text) =>
{
    return text.split("\\?").map((str) =>
    {
        return str
            .replace("??=", "#")
            .replace("??/", "\\")
            .replace("??'", "^")
            .replace("??(", "[")
            .replace("??)", "]")
            .replace("??!", "|")
            .replace("??<", "{")
            .replace("??>", "}")
            .replace("??-", "~");
    })
        .join("?");
};

const codepoints = (input) =>
{
    const octal = new RegExp("\\\\(\\d{1,3})", "g");

    let match;

    while (match = octal.exec(input))
    {
        const a = input.substring(0, match.index);
        const b = "n";
        const c = input.substring(match.index + match[0].length);
        // console.log(a + b + c);
    }

    // octal.forEach(console.log);

    // String.fromCharCode()
};

codepoints("\\\\1\\12\\123\\1224");

export const string = (text) =>
{
    const split = text.split("\\\\");

    const replaced = split.map((str) =>
    {
        return trigraphs(str)
            .replace("\\n", "\n")
            .replace("\\a", "\x07")
            .replace("\\b", "\x08")
            .replace("\\f", "\x0c")
            .replace("\\r", "\x0d")
            .replace("\\t", "\x09")
            .replace("\\v", "\x0b")
            .replace("\\'", "\x27")
            .replace("\\\"", "\x22");

        // add number ones
    });

    const joined = replaced.join("\\");

    console.log(split, replaced, joined);

    return joined;
};

export const typestring = (str) =>
{
    if (typeof str !== "string") throw "Invalid typestring";

    return str;
};

export const identifier = (str) =>
{
    if (typeof str !== "string") throw "Invalid identifier";

    return str;
};

export const literal = (str) =>
{
    if (typeof str !== "string") throw "Invalid literal";
    str = str.trim();

    const string_regex = /^\".*\"$/;

    if (string_regex.test(str))
    {
        return str.substring(1, str.length - 1);
    }

    return parseFloat(str);
};

/* eslint-env node */

module.exports = {
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended-type-checked",
        "plugin:@typescript-eslint/strict-type-checked",
        "plugin:compat/recommended",
    ],

    "parser": "@typescript-eslint/parser",

    "plugins": [
        "@typescript-eslint",
        "@stylistic",
        "compat",
    ],

    "root": true,

    "env": {
        "browser": true,
    },

    "ignorePatterns": [],

    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module",
        "warnOnUnsupportedTypeScriptVersion": false,
        "project": true,
    },

    "rules": {
        "no-magic-numbers": "off",
        "@typescript-eslint/no-magic-numbers": [ "error", {
            "ignore": [-1, 0, 1, 2, 1000],
            "ignoreEnums": true,
            "ignoreTypeIndexes": true,
        } ],

        "require-unicode-regexp": "error",

        // "no-console": "error",

        "complexity": ["error", 15],

        "require-atomic-updates": "error",

        "max-classes-per-file": "error",

        "@typescript-eslint/no-unused-vars": [ "error", {
            "args": "all",
            "argsIgnorePattern": "^_",
            "destructuredArrayIgnorePattern": "^_",
            "ignoreRestSiblings": true,
        } ],

        "eqeqeq": ["error", "smart"],

        "@typescript-eslint/no-invalid-void-type": [ "error", {
            "allowAsThisParameter": true,
        } ],

        "@typescript-eslint/no-confusing-void-expression": ["error", {
            "ignoreVoidOperator": true,
        }],

        "@typescript-eslint/no-meaningless-void-operator": "off",

        "@typescript-eslint/prefer-literal-enum-member": [ "error", {
            "allowBitwiseExpressions": true,
        }],

        "@stylistic/no-extra-semi": "error",

        "@stylistic/semi": "error",

        "@stylistic/semi-spacing": "error",

        "@stylistic/semi-style": "error",

        "@stylistic/indent": ["error", 4, {
            "SwitchCase": 1,
        }],

        "@stylistic/no-tabs": "error",

        "@stylistic/quotes": "error",

        "@stylistic/linebreak-style": "error",

        "unicode-bom": "error",

        "@stylistic/no-multi-spaces": "error",

        "@stylistic/no-multiple-empty-lines": "error",

        "@stylistic/no-trailing-spaces": "error",

        "@stylistic/eol-last": "error",

        "@stylistic/max-statements-per-line": "error",

        "@stylistic/max-len": ["error", { "code": 120 }],

        "@stylistic/padded-blocks": ["error", "never"],

        "@stylistic/padding-line-between-statements": [
            "error",
            { "blankLine": "always", "prev": "*", "next": "return" },
            { "blankLine": "always", "prev": "*", "next": "throw" },
            { "blankLine": "always", "prev": "*", "next": "continue" },
            { "blankLine": "always", "prev": "*", "next": "break" },

            { "blankLine": "always", "prev": "directive", "next": "*" },

            { "blankLine": "always", "prev": "const", "next": "*" },
            { "blankLine": "always", "prev": "*", "next": "const" },
            { "blankLine": "any", "prev": "singleline-const", "next": "singleline-const" },

            { "blankLine": "always", "prev": "let", "next": "*" },
            { "blankLine": "always", "prev": "*", "next": "let" },
            { "blankLine": "any", "prev": "singleline-let", "next": "singleline-let" },

            { "blankLine": "always", "prev": "*", "next": "class" },
            { "blankLine": "always", "prev": "class", "next": "*" },

            { "blankLine": "always", "prev": "*", "next": "export" },
            { "blankLine": "always", "prev": "export", "next": "*" },

            { "blankLine": "always", "prev": "multiline-block-like", "next": "*" },
            { "blankLine": "always", "prev": "multiline-expression", "next": "*" },
            { "blankLine": "always", "prev": "*", "next": "multiline-block-like" },
            { "blankLine": "always", "prev": "*", "next": "multiline-expression" },
        ],

        "curly": ["error", "multi-line", "consistent"],

        "@stylistic/brace-style": ["error", "allman"],

        "@stylistic/nonblock-statement-body-position": ["error", "below", { "overrides": { "if": "beside" } }],

        "@typescript-eslint/explicit-member-accessibility": "error",

        "@stylistic/block-spacing": "error",

        "@stylistic/comma-spacing": "error",

        "@stylistic/comma-style": "error",

        "@stylistic/dot-location": ["error", "property"],

        "@stylistic/func-call-spacing": "error",

        "@stylistic/function-call-argument-newline": ["error", "consistent"],

        "@stylistic/function-paren-newline": ["error", "multiline-arguments"],

        "@stylistic/keyword-spacing": "error",

        "@stylistic/lines-between-class-members": "error",

        "@stylistic/newline-per-chained-call": "error",

        "@stylistic/operator-linebreak": ["error", "before"],

        "@stylistic/object-property-newline": ["error", { "allowAllPropertiesOnSameLine": true }],

        "@stylistic/object-curly-newline": ["error", { "consistent": true }],

        "@stylistic/type-annotation-spacing": "error",

        "@stylistic/array-bracket-newline": ["error", "consistent" ],

        "@stylistic/arrow-spacing": "error",

        "@stylistic/lines-around-comment": [ "error", {
            "beforeBlockComment": true,
            "allowBlockStart": true,
            "allowObjectStart": true,
            "allowArrayStart": true,
            "allowClassStart": true,
            "allowEnumStart": true,
            "allowInterfaceStart": true,
            "allowModuleStart": true,
            "allowTypeStart": true,
        }],
    },
};

import { inspect } from "node:util";
import { defineConfig, loadEnv, UserConfigExport } from "vite";

const DEV = "DEVELOPMENT";
const PROD = "PRODUCTION";

const RESET = 0, BOLD = 1, DIM = 2;
const RED = 31, GREEN = 32, BLUE = 34;

const buildMode = (mode: string) =>
{
    const $ = (...n: unknown[]) => `\x1b[${n.join(";")}m`;

    const hi = (mode === DEV)
        ? $(BOLD, BLUE)
        :(mode === PROD)
            ? $(BOLD, GREEN)
            :$(BOLD, RED);

    const dim = $(RESET, DIM);
    const reset = $(RESET);

    return `${dim}THIS IS A ${hi}${mode}${dim} BUILD${reset}`;
};

const print = (...data: unknown[]) =>
{
    const format = (object: unknown) =>
    {
        if (typeof object === "string") return object;

        return inspect(
            object,
            { depth: null, colors: true, sorted: true, }
        )
            .split("\n")
            .map(l => l.trim())
            .join(" ");
    };

    data.forEach((entry) =>
    {
        console.log(format(entry));
        console.log();
    });
};

export default defineConfig(({
    command,
    mode,
}) =>
{
    mode = mode.toUpperCase();

    const config: UserConfigExport = {
        build: {
            minify: false,

            rollupOptions: {
                treeshake: {
                    preset: "smallest",
                    moduleSideEffects: true,
                },
                output: {
                    validate: true,
                }
            },
        },

        esbuild: mode === PROD
            ? {
                drop: [ "debugger" ],
                keepNames: true,
            }
            : {},

        server: { host: command === "serve" },
    };

    const env = loadEnv(mode, process.cwd(), "");

    print(env, buildMode(mode), config);

    return config;
});
